﻿using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Web.Helpers;
using Isi2.Projekt.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Isi2.Projekt.Web.Controllers
{
    public class CategoryController : Controller
    {
        private IService service = WindsorContainerAccess.GetContainer().Resolve<IService>();

        //
        // GET: /Category/
        [OutputCache(Duration = 10, VaryByParam = "none")]
        public PartialViewResult Index()
        {
            return PartialView(service.GetCategories(null, null));
        }

        //
        // GET: /Category/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Category/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Category/Create

        [HttpPost]
        public ActionResult Create(CategoryModel model)
        {
            if (ModelState.IsValid)
            {
                SaveContract wynik = service.Create(new CategoryContract()
                {
                    Name = model.CategoryName
                });

                if (wynik.Errors != "")
                {
                    ModelState.AddModelError("", wynik.Errors);
                }

                return RedirectToAction("Index");
            }
            return View(model);
        }

        //
        // GET: /Category/Delete/5

        //public ActionResult Delete(int id)
        //{
        //    return View();
        //}

        ////
        //// POST: /Category/Delete/5

        //[HttpPost]
        //public ActionResult Delete(int id, CategoryModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        string wynik = service.Delete(id);

        //        if (wynik != "")
        //        {
        //            ModelState.AddModelError("", wynik);
        //        }

        //        return RedirectToAction("Index");
        //    }
        //    return View(model);
        //}

        //
        // GET: /Category/AddCategories

        public ActionResult AddCategories()
        {
            return View(service.GetCategories(null, null));
        }

        //
        //POST: /Category/AddCategories

        [HttpPost]
        public ActionResult AddCategories(int entryId)
        {
            if (ModelState.IsValid)
            {
                var categories = service.GetCategories(null, null).Items;
                List<CategoryContract> lista = new List<CategoryContract>();
                foreach (var category in categories)
                {
                    if (Request.Form[category.Name] == "on")
                        lista.Add(category);
                }
                SaveContract wynik = service.AddEntryCategories(entryId, lista);

                if (wynik.Errors != "")
                {
                    ModelState.AddModelError("", wynik.Errors);
                }

                return RedirectToAction("Index");
            }
            return View();
        }
    }
}
