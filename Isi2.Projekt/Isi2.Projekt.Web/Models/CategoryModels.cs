﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Isi2.Projekt.Web.Models
{
    public class EntryCategoriesModel
    {
        public bool IsAdded { get; set; }

        public string CategoryName { get; set; }
    }

    public class CategoryModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Nazwa kategorii")]
        public string CategoryName { get; set; }
    }
}