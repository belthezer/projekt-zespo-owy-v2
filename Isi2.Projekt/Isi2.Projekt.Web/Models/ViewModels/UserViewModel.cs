﻿using Isi2.Projekt.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Isi2.Projekt.Contracts.Common;


namespace Isi2.Projekt.Web.Models.ViewModels
{
    public class UserViewModel
    {
        public PagedListResultContract<EntryContract> Entries { get; set; }

        public UserContract User { get; set; }

    }
}
