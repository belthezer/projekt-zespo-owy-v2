﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace Isi2.Projekt.Web.Models
{

    public class ImageUploadModel
    {
        [Required]
        [Display(Name = "Użytkownik")]
        public string AuthorId { get; set; }
    }
}
