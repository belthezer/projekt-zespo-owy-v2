﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts.User;

namespace Isi2.Projekt.Web.Helpers
{
    public class AccessGroupSecurityProvider
    {
        private IService _service;

        public AccessGroupSecurityProvider()
        {
             _service = WindsorContainerAccess.GetContainer().Resolve<IService>();
        }
        private static AccessGroupSecurityProvider _instance;
        public static AccessGroupSecurityProvider Instance
        {
            get { return _instance ?? (_instance = new AccessGroupSecurityProvider()); }
        }

        public string GenerateHash(int userId)
        {
            var user = _service.GetUsers(new UserFilterContract() {Id = userId}, null).Items.FirstOrDefault();
            if (user!=null)
            {
                string input = string.Concat(user.Id, user.Email, user.Password, user.Password_reminder, user.Login,
                                         user.Date_created);
                return GetHash(input);
            }
            return "-1";
        }

        public bool ValidateHash(int userId, string hash)
        {
            bool result = GenerateHash(userId).Equals(hash, StringComparison.InvariantCultureIgnoreCase);
            return result;
        }

        private string GetHash(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}