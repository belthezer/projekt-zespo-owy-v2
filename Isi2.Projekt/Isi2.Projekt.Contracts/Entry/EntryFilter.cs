﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Isi2.Projekt.Contracts.Entry
{
    [DataContract]
    public class EntryFilterContract
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public List<int> Ids { get; set; }

        [DataMember]
        public int? CategoryId { get; set; }

        [DataMember]
        public bool? Published { get; set; }

        [DataMember]
        public bool? Deleted { get; set; }

        [DataMember]
        public int Page { get; set; }

        [DataMember]
        public bool Sticky { get; set; }

        [DataMember]
        public int? UserId { get; set; }

		[DataMember]
        public string Username { get; set; }
    }
}
