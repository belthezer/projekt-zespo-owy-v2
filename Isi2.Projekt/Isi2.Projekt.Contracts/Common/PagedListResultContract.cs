
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Isi2.Projekt.Contracts.Common
{
    [DataContract]
    public class PagedListResultContract<T> where T : class
    {
        [DataMember]
        public List<T> Items { get; set; }

        [DataMember]
        public int Count { get; set; }

        [DataMember]
        public bool Success { get; set; }

        [DataMember]
        public string[] Errors { get; set; }

        [DataMember]
        public int Page { get; set; }
    }
}
