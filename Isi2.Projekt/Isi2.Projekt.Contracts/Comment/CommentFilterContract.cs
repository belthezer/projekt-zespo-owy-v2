﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Isi2.Projekt.Contracts.Comment
{
    [DataContract]
    public class CommentFilterContract
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public int? EntryId { get; set; }

        [DataMember]
        public int Page { get; set; }
    }
}
